
import time
import subprocess
from subprocess import PIPE


def execute_one(cmd):
    # Start
    one = subprocess.Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

    # Send new line to stop program and get outputs
    stdout, stderr = one.communicate(input=b'\n') # Sends new line to client ending it

    # Debug messages in case of test failure
    print('stdout:\n' + stdout.decode('UTF-8'))
    print('stderr:\n' + stderr.decode('UTF-8'))

    return {
        "stdout": stdout,
        "stderr": stderr,
    }


def execute_server_and_client(server_cmd, client_cmd):
    # Start server
    server = subprocess.Popen(server_cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    # Wait for server to start
    time.sleep(0.1)
    # Start client
    client = subprocess.Popen(client_cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

    # Send new line to stop program and get outputs
    client_stdout, client_stderr = client.communicate(input=b'\n') # Sends new line to client ending it
    server_stdout, server_stderr = server.communicate(input=b'\n') # Sends new line to server ending it

    # Debug messages in case of test failure
    print('server_stdout:\n' + server_stdout.decode('UTF-8'))
    print('server_stderr:\n' + server_stderr.decode('UTF-8'))
    print('client_stdout:\n' + client_stdout.decode('UTF-8'))
    print('client_stderr:\n' + client_stderr.decode('UTF-8'))

    return {
        "server_stdout": server_stdout,
        "server_stderr": server_stderr,
        "client_stdout": client_stdout,
        "client_stderr": client_stderr,
    }


def test_correct_execution():
    """Test if correct code is executed"""
    server_cmd = './build/app -server'
    client_cmd = './build/app -mode e -file example-inputs/ok.go'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)

    # Assert corect outputs
    assert b'Code to execute:' in results["server_stdout"]
    assert b'Execution result: ok' in results["client_stdout"]


def test_no_server():
    """Tests if client fails with no server"""
    client_cmd = './build/app -mode e -file example-inputs/ok.go'

    results = execute_one(cmd=client_cmd)

    # Assert corect outputs
    assert b'Execution result: ok' not in results["stdout"]


# Test functions begin with "test_"

def test_compilation_succeeded():
    """Tests if code was compiled successfully."""
    server_cmd = './build/app -server'
    client_cmd = './build/app -mode=c -file=example-inputs/ok.go'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)

    assert b'Compilation succeeded.' in results["client_stdout"]


def test_compilation_failed():
    """Tests if code failed to compile."""
    server_cmd = './build/app -server'
    client_cmd = './build/app -mode=c -file=example-inputs/bad.go'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)

    assert b'Compilation failed:' in results['client_stdout']


def test_statistic_succeeded():
    """Tests if statistics are send succesfully."""
    server_cmd = './build/app -server'
    client_cmd = './build/app -mode=e -file=example-inputs/ok.go'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)

    server_cmd = './build/app -server'
    client_cmd = './build/app -statistics'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)
    
    assert b'nazwa pliku:' in results['client_stdout']

def test_handle_wrong_argument():
    """Test if information about wrong command argument is printed out."""

    server_cmd = './build/app -server'
    client_cmd = './build/app -noExistingFlag'

    results = execute_server_and_client(server_cmd=server_cmd, client_cmd=client_cmd)
    
    assert b'flag provided but not defined: -noExistingFlag' in results['client_stderr']
   
