# HOW TO EXECUTE:

1. Have go installed
2. Go to cloned repo `cd go-rpc`
3. Build `./build.sh`
5. Execute both server and client:
    * first terminal: `./build/app -server`
    * second terminal: `./build/app -mode e -file "path_to_file"`

# EXAMPLE CLIENT EXECUTIONS:

With no parameters client

1. `./build/app -mode e -file "example-inputs/ok.go"` - correct input
2. `./build/app -file "somethingthatdoesntexist.go"` - invalid input

# HOW TO RUN TESTS:

1. Execute `./test.sh`
2. Install pytest `pip3 install --user pytest`
3. Execute python tests `pytest`
