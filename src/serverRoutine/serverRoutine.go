package serverRoutine

import "net/http"
import "log"
import "net"
import "net/rpc"
import "remoteCode"
import ( "fmt"
         "bufio"
         "os"
       )

func Main(serverAddres, port string) {
	
	remotecode := new(remoteCode.Int)
	rpc.Register(remotecode)
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", serverAddres+":"+port)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
	
	fmt.Print("Press 'Enter' to end...\n")
        bufio.NewReader(os.Stdin).ReadBytes('\n')
		
}
