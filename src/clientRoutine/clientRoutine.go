package clientRoutine

import (
    "bufio"
    "fmt"
    "github.com/matryer/goscript"
    "log"
    "net/rpc"
    "os"
)

func CompileAndPrint(serverAddress, port, codeToExecute string) {
    client, err := rpc.DialHTTP("tcp", serverAddress + ":" + port)
    if err != nil {
        log.Fatal("dialing:", err)
    }

    var gosErr *goscript.Error
    _ = client.Call("Int.Compile", &codeToExecute, &gosErr);

    if errMsg := gosErr.Error(); errMsg != "" {
        fmt.Printf("%v\n", errMsg)
    } else {
        fmt.Printf("Compilation succeeded.\n")
    }

    fmt.Print("Press 'Enter' to end...")
    bufio.NewReader(os.Stdin).ReadBytes('\n')
}

func ExecuteAndPrint(serverAddress, port string, codeToExecute string) {

	client, err := rpc.DialHTTP("tcp", serverAddress + ":" + port)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	
	var reply interface{}

	err = client.Call("Int.Execute", &codeToExecute, &reply)
	if err != nil {
		log.Fatal("remoteCode error: ", err)
	}

  fmt.Printf("Execution result: %v (type: %T)\n", reply, reply)

  fmt.Print("Press 'Enter' to end...")
  bufio.NewReader(os.Stdin).ReadBytes('\n')

}

func CollectStatisticsAndPrint(serverAddress, port string) {

	client, err := rpc.DialHTTP("tcp", serverAddress + ":" + port)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	
	var reply interface{}

	err = client.Call("Int.SendReport", &port, &reply)  // TODO insert proper function name here, also return data to proper type
	if err != nil {
		log.Fatal("remoteCode error: ", err)
	}

    fmt.Printf("Server statistics: %v (type: %T)\n", reply, reply)

}
