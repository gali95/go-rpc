package main

import "os"
import "log"
import "flag"
import "clientRoutine"
import "serverRoutine"
import "io/ioutil"

func main() {

	server := flag.Bool("server", false, "Run server instead of client")
	address := flag.String("ip", "", "What is ip address of server")
	port := flag.String("port", "", "What port is used")
	filepath := flag.String("file", "", "Path to file with code")
	clientMode := flag.String("mode", "c", "c - compile, e - execute")
	statistics := flag.Bool("statistics",false, "Client collects data from server")

	flag.Parse()

	if *port == "" {
		*port = os.Getenv("SERVER_PORT")
	}
	if *port == "" {
		*port = "1234"
	}

	if *address == "" {
		*address = os.Getenv("SERVER_ADDRESS")
	}

	log.Print("SERVER_PORT: ", *port)	
    if *server {
		log.Print("SERVER_ADDRESS: ", *address)	
		log.Print("Server: ")
	    serverRoutine.Main(*address, *port)
    } else {
		if *statistics {
			log.Print("STATISTICS: ")
			clientRoutine.CollectStatisticsAndPrint(*address, *port)
			os.Exit(0)
		}
		code, codeError := filePathToCode(*filepath)
		if codeError != nil {
			log.Print("Readfile error: ",codeError)
		} else {
			if *address == "" {
				*address = "127.0.0.1"
			}
			log.Print("SERVER_ADDRESS: ", *address)	
			log.Print("Client: ")
			if *clientMode == "c" {
				log.Print("Compilation: ")
				clientRoutine.CompileAndPrint(*address, *port, code)
			} else if *clientMode == "e" {
				log.Print("Execution: ")
				clientRoutine.ExecuteAndPrint(*address, *port, code)
			} else {
				log.Print("Inproper client mode (only c and e allowed)")
			}
		}
    }

}

func filePathToCode(filepath string) (string,error) {

	b, err := ioutil.ReadFile(filepath)
	if err != nil {
		return "",err
	}

	return string(b),nil
}
