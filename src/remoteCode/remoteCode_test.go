package remoteCode

import (
    "testing"
    "github.com/matryer/goscript"
    "github.com/stretchr/testify/assert"
    "os"
)

func TestExecuteCodeSuccess(t *testing.T) {
	code := `
        func goscript() (string, error) {
            return "ok", nil
        }
    `

	var result interface{}

	ExecuteCode(&code, &result);

	r := result.(string)

	if r != "ok" {
		t.Errorf("Expected result: ok, got: %s", result)
	}
}

func TestExecuteCodeFailure(t *testing.T) {
	code := `
        func go() (string, error) {
            return "ok", nil
        }
    `

	var result interface{}

	ExecuteCode(&code, &result);

	if result != nil {
		t.Errorf("Expected result: nil, got: %s", result)
	}
}

func TestCompilationSucceeded(t *testing.T) {
    code := `
          func goscript() (string, error) {
              return "Compilation succeeded.", nil
          }
    `
    var gosErr goscript.Error

    CompileCode(&code, &gosErr);
    assert.Empty(t, gosErr.Error())
}

func TestCompilationFailed(t *testing.T) {
    code := `
          func goscript() (string, error) {
          }
    `
    var gosErr goscript.Error

    CompileCode(&code, &gosErr);
    assert.NotEmpty(t, gosErr.Error())
}

func TestGetFileName(t *testing.T) {
    
    nazwa_pliku, title := GetFileName()
   
    expected_nazwa_pliku := "src/archives/" + title
	if nazwa_pliku != expected_nazwa_pliku {
		t.Errorf("Expected result: %s, got: %s", expected_nazwa_pliku, nazwa_pliku)
	}    
}

func TestGetFileNameExists(t *testing.T) {
    
    nazwa_pliku, _ := GetFileName()
    
    _, err := os.Stat(nazwa_pliku); os.IsNotExist(err)
    
	if err == nil {
        t.Errorf("Expected result: file do not exist, got: file exist")
    }   
}

func TestSaveToFile(t *testing.T) {
    verses := `
          func goscript() (string, error) {
              return "Compilation succeeded.", nil
          }
    `
    nazwa_pliku := "badpath/a";
    
    err := SaveToFile(verses,nazwa_pliku)
    
	if err == nil {
        t.Errorf("Not Expected result: %v", err)
    }   
}

func TestAnalizeStatistics(t *testing.T) {
    verses := `a1,b2,c3`
    
    k_s, char_a := AnalizeStatistics(verses)
    
    e_k_s := "1"
    e_char_a := "8"
    
    if e_k_s != k_s {
        t.Errorf("Expected result: %s, got: %s", e_k_s, k_s)
    }
    
    if e_char_a != char_a {
        t.Errorf("Expected result: %s, got: %s", e_char_a, char_a)
    }
}