package remoteCode

import (
    "fmt"
    "github.com/matryer/goscript"
    "os"
    "strconv"
    "io/ioutil"
    
)

type Int int

func (t *Int) Compile(code *string, gosErr *goscript.Error) error {
    CompileCode(code, gosErr)
    return nil
}

func CompileCode(code *string, gosErr *goscript.Error) {
    fmt.Printf("Code to compile:\n%v", *code)

    script := goscript.New(*code);
    defer script.Close()

    if _, err := script.Execute(); err != nil {
        gosErr.Stderr = fmt.Sprintf("Compilation failed: %v", err.Error())
    }
}

func (t *Int) Execute(code *string, result *interface{}) error {
    ExecuteCode(code, result);
    Statistics(code);
	return nil
}

func ExecuteCode(code *string, result *interface{}) {
    fmt.Printf("Code to execute:\n%v", *code)

	script := goscript.New(*code);
	defer script.Close()
	res, err := script.Execute();
	if err != nil {
    fmt.Println("ERROR: ", err)
	}
	*result = res
}


func GetFileName() (string, string) {
    
    var numer_pliku int =1;
    var nazwa_pliku string;
    var title string;
    
    
    for {
        title = "plik" + strconv.Itoa(numer_pliku) +".go"
        nazwa_pliku = "src/archives/" + title
        
        _, err3 := os.Stat(nazwa_pliku); os.IsNotExist(err3)
    
        // sprawdza czy istnieje juz plik o danym numerze
        if err3 == nil {
            numer_pliku++;
        } else {
            break
        }
    }
     return nazwa_pliku, title
}

func SaveToFile(verses string, nazwa_pliku string) error {
    
//////////////// magazynowanie //////////////////////////
    file, err2 := os.Create(nazwa_pliku) 
    if err2 != nil {
        fmt.Println("Blad przy tworzeniu pliku w archiwum")
        return err2
    }
    
    file.Write([]byte(verses))
    
    file.Close()
    
    return nil
}

func AnalizeStatistics(verses string) (k_s string, char_a string) {
//////////////////////////// analiza kodu ////////////////////////
    var k int = 0;
    
    for i := 0; i < len(verses); i++ {
        
        if string(verses[i]) == "\n" {
            k++
        } 
    }
    
    k++
    k_s = strconv.Itoa(k)
    char_a =strconv.Itoa(len(verses))
    
    return 
}

func Statistics(code *string) {
    fmt.Printf("Generating statistics\n")
    
    verses := string(*code)
    
    nazwa_pliku, title := GetFileName()
    err2 := SaveToFile(verses,nazwa_pliku)
        if err2 != nil {
            fmt.Printf("Bład przy SaveToFile")
        } 
    k_s, char_a := AnalizeStatistics(verses)
    
//////////////////////// zapis analizy do pliku txt wersja podstawowa //////////////////
  
    raport_name := "src/archives/raport.txt"
    
    file, err := os.OpenFile(raport_name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0755)
    if err != nil {
          fmt.Printf("Błąd podczas otwierania pliku: %s",raport_name)
          os.Exit(1)
    }
    
    //linijka I
    file.Write([]byte("nazwa pliku: "))
    file.Write([]byte(title))
    file.Write([]byte("\n"))  
   
    // linijka II
    file.Write([]byte("lini: "))
    file.Write([]byte(k_s))
    file.Write([]byte("\n"))
    
    //linijka III
    file.Write([]byte("znakow: "))
    file.Write([]byte(char_a))
    file.Write([]byte("\n\n"))
       
    file.Close() 
} 

func (t *Int) SendReport(code *string, result *interface{}) error{

    // konwersja raportu do stringa
    raport_name := "src/archives/raport.txt"
    
    file2, err2 := ioutil.ReadFile(raport_name)
    if err2 != nil {
        fmt.Printf("Błąd")
        os.Exit(1)
    }
    
    *result = string(file2)

    //fmt.Printf("Sending statistics:\n%v", *result)
    
    return nil;

}


