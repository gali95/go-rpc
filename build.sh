#!/usr/bin/env bash

export GOPATH="$(pwd)"
echo "GOPATH=$GOPATH"
go get github.com/matryer/goscript
go get github.com/stretchr/testify/assert
mkdir -p "build"
mkdir -p "src/archives"
go build -o build/app appMain
